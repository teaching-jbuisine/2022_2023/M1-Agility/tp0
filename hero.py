from character import Character

import random


class Hero(Character):
    """Default hero character
    """

    def __init__(self, name, life, power):
        super().__init__("Hero", name, life, power)

    def fight(self):
        # by default hero always has 
        return random.randint(5, 10) * self.power

    def update_stats(self):
        # by default the hero will always hit harder
        self.power *= 1.2