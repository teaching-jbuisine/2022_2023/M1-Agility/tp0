from character import Character

import random

class Monster(Character):
    """Default monster character
    """

    def __init__(self, name, life, power):
        super().__init__("Monster", name, life, power)

    def fight(self):
        # by default monster can inflict huge hit 
        return random.randint(0, 20) * self.power

    def update_stats(self):
        # by default the monster not always gain in power
        if random.random() > 0.5:
            self.power *= 1.05