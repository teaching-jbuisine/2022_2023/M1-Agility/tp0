# FightGame

## Description

Simulation de combats entre monstres et héros ! Qui l'emportera ?

## Prérequis

Quelques petits points importants avant de commencer votre mission :
- Ajouter votre groupe dans ce [document](https://docs.google.com/spreadsheets/d/1oupddN1emZKVzsNHxGebZF0r2wLY4oIbG5c3atLL450/edit?usp=sharing) partagé. Attention à l'onglet sélectionné ! Il vous faudra également renseigner les `hash` des commits liés à chacune des versions du projet.

- Ajouter mon compte `jbuisine` en tant que rapporteur sur le projet **Gitlab** du groupe.
## Votre mission

Le livreur possédera toujours la même procédure :

- Récupérer les modifications des branches
- Mettre à jour le fichier de `fight.py` pour faire combattre les nouveaux personnages.
- Valider les tâches des développeurs dans ce fichier par en modifiant l'item `[ ]` par `[X]`.
- Réaliser un commit pour les deux tâches précédentes (validation développeurs et mise à jour README.md).
- Réaliser un tag de livraison sous la forme `vX.0.0`.

Le développeur devra:

- Créer une branche `feature/StepX_devY` où `X` correspond à l'étape de développement et `Y` le développeur.
- Réaliser un commit dans cette nouvelle branche pour chaque tâche développée.

**Étape 1:**

- *Développeur 1:*
    - [ ] Ajouter dans le fichier `monster.py` une nouvelle classe `Dragon` héritant des comportements de la classe `Monster`.
    - [ ] La classe a la particularité de posséder 30% de chance d'esquiver les dégâts subit lors d'un tour de combat.
    - [ ] Elle possède 400 points de vie et une force de 1 au début.

- *Développeur 2:*
  - [ ] Ajouter dans le fichier `hero.py` la classe `Mage` héritant par défaut des comportements de la classe `Hero`.
  - [ ] La classe peut possèder un sort de dégât allant de 20 à 40 mais son pouvoir n'augmente pas.
  - [ ] Elle possède 200 points de vie et une force de 1 (qui sera donc fixe).

**Étape 2:**

- *Développeur 1:*
    - [ ] Ajouter dans le fichier `monster.py` une nouvelle classe `Orc` héritant des comportements de la classe `Monster`.
    - [ ] La classe a la particularité de pouvoir en moment de "rage intense" (désolé je ne trouvais pas mieux) d'infliger 10 fois les dégâts. Cela à hauteur de 5% de probabilité.
    - [ ] Elle possède 120 points de vie et une force de 3 au début.

- *Développeur 2:*
  - [ ] Ajouter dans le fichier `hero.py` la classe `Knight` héritant par défaut des comportements de la classe `Hero`.
  - [ ] La classe peut possèder un sort de protection qui permet de ne subir que 50% des dégâts les 3 premiers tours.
  - [ ] Elle possède 100 points de vie et une force de 2.

**Étape 3: amusez-vous !**

En réalité, chaque développeur va créer sa propre classe monstre / héro. Le livreur sera en fait l'arbitre du combat !

Voici les règles à **respecter** :
- La nouvelle classe peut hériter de l'une des nouvelles classes définit dans les étapes 1 et 2 ou simplement la classe initiale.

- En plus, le développeur peut choisir l'une des caractéristiques suivantes supplémentaires :
  - Soit un maximum de vie de 1000.
  - Soit un bonus multiplicateur de 3 de dégâts.
  - Soit une esquive de 40%.
  - Soit une probabilité de 3% de multiplier les dégâts par 20.

Faites vos jeux !

**Règle:** Le livreur (après avoir livré la dernière version) réalisera 5 simulations, le gagnant sera celui avec un minimum de 3 victoires !